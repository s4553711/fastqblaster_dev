package com.ck.fastq;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class FastqByteReader {
	private boolean debug = true;
	private InputStream input;
	private ByteBuffer buffer;

	public FastqByteReader(InputStream in) {
		input = in;
		buffer = ByteBuffer.allocate(1024 * 1024 * 50);
	}
	
	public byte[] hasNext() {
		byte[] a = new byte[1024 * 8];
		byte[] b = null;
		byte[] result = null;
		int nRead;
		int new_line = 0, start = 0, end = 0, i = 0;
		if (debug) System.out.println("called");
		try {
			if ((nRead = input.read(a, 0, a.length)) != -1) {
                new_line = 0;
                start = 0;
                end = 0;
                i = 0;
                if (buffer.position() != 0) {
                    if (debug) System.out.println("1-1>"+buffer);
                    buffer.put(Arrays.copyOfRange(a, 0, nRead));
                    if (debug) System.out.println("1-2>"+buffer);
                    buffer.flip();
                    b = new byte[buffer.remaining()];
                    buffer.get(b);
                    if (debug) System.out.println("1-3>"+b.length);
                    buffer.clear();
                    if (debug) System.out.println("1-4>"+buffer);
                } else {
                	if (debug) System.out.println("2");
                    b = Arrays.copyOfRange(a, 0, nRead);
                }
                for (i = 0; i < b.length; i++) {
                    if (b[i] == 10) {
                        new_line += 1;
                        if (debug) System.out.println("nRead: "+nRead);
                        if (debug) System.out.println("new line "+new_line);
                        if (debug) System.out.println("DD> "+i+" / "+b.length);
                        if (( i == (b.length - 1) || b[i + 1] == 64 ) && new_line == 4) {
                            end = i;
                            if (debug) System.out.println("W1> "+start+" - "+end);
                            
                            //this.queue.put(Arrays.copyOfRange(b, start, end));
                            result = Arrays.copyOfRange(b, start, end);
                            
                            if (debug) System.out.println("stor> "+new String(Arrays.copyOfRange(b, start, end)));
                            start = i + 1;
                            new_line = 0;
                            
                            break;
                        }
                    }
                    if (b[i] == 0) {
                        end = i;
                        break;
                    }
                }
                if (debug) System.out.println("before adjust> "+start+" - "+end+" : "+i);
                if (end == 0) 
                	end = i - 1;
                if (debug) System.out.println("after adjust> "+start+" - "+end+" : "+i);
                if (end < (nRead - 1)) {
                	if (debug) System.out.println("pick up to buffer");
                	buffer.put(Arrays.copyOfRange(b, start, nRead));
                	if (debug) System.out.println(buffer);
                }
			} else {
				buffer.flip();
				if (debug) System.out.println("QQ "+buffer);
				if (buffer.remaining() == 0) {
					if (debug) System.out.println("QQ2 "+buffer);
					result = new byte[]{};
				} else {					
					result = new byte[buffer.remaining() - 1];
					if (debug) System.out.println("C1> "+buffer);
					buffer.get(result);
					if (debug) System.out.println("C2> "+buffer);
					if (debug) System.out.println("C3> "+result.length);
					if (debug) System.out.println("C4>\n"+new String(result));
					buffer.clear();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (result == null)
			result = new byte[]{};
		
		if (debug) System.out.println("before back: "+new String(result)+"<");
		return result;
	}
	
	public void close() throws IOException {
		input.close();
	}
}
