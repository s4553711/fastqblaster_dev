package com.ck;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Arrays;
import java.util.Date;

public class NioRunner {
	public static void main(String[] args) {
		SocketChannel client[] = new SocketChannel[args.length];
		try {
			for (int j = 0; j < args.length; j++) {
				String[] argPart = args[j].split(":");
				if (argPart.length < 2) {
					System.out.println("Illegal argument.");
					return;
				}
				client[j] = SocketChannel.open();
				client[j].connect(new InetSocketAddress(argPart[0], Integer.valueOf(argPart[1])));
				client[j].configureBlocking(false);
			}
			BufferedInputStream bufferedInputStream = new BufferedInputStream(System.in);
			int c = 0;
			byte[] data = new byte[1024 * 1024 * 16];
			int read = 0;
			int i = 0;
			int start = 0;
			int end = 0;
			int count_new_line = 0;
			ByteBuffer buffer = ByteBuffer.allocate(1024 * 1024 * 20);
			while ((read = bufferedInputStream.read(data, 0 , data.length)) != -1) {
				//Date d1 = new Date(); 
				start = 0;
				end = 0;
				i = 0;
				for (i = 0; i < read; i++) {
					if (data[i] == 10) {
						count_new_line += 1;
						if (data[i+1] == 64 && count_new_line == 1500 * 10) {
							end = i + 1;
							//System.out.println("1> from "+start+" to "+end+" / "+read);
							buffer.put(Arrays.copyOfRange(data, start, end));
							buffer.flip();	
							while(buffer.hasRemaining()) {
								client[c % client.length].write(buffer);
							}
							buffer.clear();
							start = i + 1;
							count_new_line = 0;
						}
					}
					if (data[i] == 0){
						end = i;
						break;
					}
				}
				if (start == end) end = i;
				//System.out.println("2> from "+start+" to "+end+" / "+read);
				buffer.put(Arrays.copyOfRange(data, start, end));
				buffer.flip();	
				while(buffer.hasRemaining()) {
					client[c % client.length].write(buffer);
				}
				buffer.clear();
				//System.out.println("diff >"+(new Date().getTime() - d1.getTime()));
				
//				System.out.println("read "+read);
//				buffer.put(Arrays.copyOfRange(data, 0, read));
//				buffer.flip();
//				while(buffer.hasRemaining()) {
//					client[c % client.length].write(buffer);
//				}
//				buffer.clear();				
				
//				System.out.println("Reads > "+read);
//				client[c % client.length].write(buffer);
//				if (buffer.hasRemaining()) {
//					System.out.println("Rman > "+buffer.remaining());
//					buffer.compact();
//				} else {
//					System.out.println("Clear > ");
//					buffer.clear();
//				}
				c++;
			}
			buffer.flip();	
			while(buffer.hasRemaining()) {
				client[0].write(buffer);
			}
			buffer.clear();
			bufferedInputStream.close();
			for (SocketChannel ch : client) {
				ch.close();
			}
		} catch (IOException e) {
			System.out.println("IOException");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
