package com.ck.handler;

import java.io.UnsupportedEncodingException;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;

public class DiscardServerHandler extends ChannelInboundHandlerAdapter {

	public void channelRead(ChannelHandlerContext ctx, Object msg) {
		//System.out.println(ctx.channel().remoteAddress() + " Say : " + msg);
		ByteBuf in = (ByteBuf) msg;
		try {
			//System.out.print(in.toString(io.netty.util.CharsetUtil.US_ASCII));
			byte[] bytes = new byte[in.readableBytes()];
			in.readBytes(bytes);
			System.out.print(new String(bytes, "US-ASCII"));

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} finally {
			ReferenceCountUtil.release(msg);
		}
		// Discard the received data silently.
        //((ByteBuf) msg).release();
    }

    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
        cause.printStackTrace();
        ctx.close();
    }
}
