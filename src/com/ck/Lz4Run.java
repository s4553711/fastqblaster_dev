package com.ck;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import net.jpountz.lz4.LZ4BlockInputStream;

public class Lz4Run {
	public static void main(String[] args) {
		System.out.println("Input is "+args[0]);
		 byte[] buf = new byte[100];
		    try {
				String outFilename = args[0];
		        LZ4BlockInputStream in = new LZ4BlockInputStream(new FileInputStream(args[0]));
		        //FileOutputStream out = new FileOutputStream(outFilename);
		        int len;
		        while((len = in.read(buf)) > 0){
		            //out.write(buf, 0, len);
		        	System.out.println("Output is:"+Arrays.toString(buf));
		        }
		        in.close();
		        //out.close();
		    } catch (IOException e) {
		    	System.out.println("2");
		    	e.printStackTrace();
		    }
	}

}
